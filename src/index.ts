import {todo} from './types/todo.js'; 
import { initialiseMyTasks} from './db/database.js';
import {commandNotFound} from './view/output.cli.js';
import {executeCommand, isValidCommand} from './controller/crud.controller.js';


(async () => {
    
let myTasks:todo[]=[];
let myArgs:string[]=process.argv.slice(2);
let id=0;
id=await initialiseMyTasks(myTasks,id);
let userCommand:string|undefined=myArgs.shift();
isValidCommand(userCommand) ? executeCommand(userCommand,myArgs,id,myTasks) : commandNotFound();

})();











