export enum Commands {
    create = "create",
    update = "update",
    show = "show",
    delete = "delete",
  }