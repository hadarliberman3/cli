import { todo } from "../types/todo.js";

export function printTable(myTasks:todo[]){
console.table(myTasks);
}

export function printIsFoundMessage(found:boolean,inputId:string,action:string){
    found?console.log(`item ${inputId} ${action}`):console.log(`item with id ${inputId} not found`);
}

export function commandNotFound(){
    console.log("command not found");

}

export function newTaskIdMessage(id:number){
    console.log(`this task id is ${id}`);
}