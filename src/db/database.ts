import fs from 'fs/promises';
import {todo} from '../types/todo.js';


export async function updateJsonFile(myTasks:todo[]){
    await fs.writeFile('./dist/todos.json',JSON.stringify(myTasks,null,2));
}

export async function initialiseMyTasks(myTasks:todo[],id:number){
    try{
        let myTasksjson=JSON.parse(await fs.readFile('./dist/todos.json','utf-8'));
    
        if(myTasksjson.length){
            for(let curtask of myTasksjson){
                let newTask: todo = {id:curtask.id, task:curtask.task, complete:curtask.complete};
                myTasks.push(newTask);
            }
        let lastIndex=myTasks.length-1;
        id=parseInt((myTasks[lastIndex].id));
        }
    }
    catch(err){
        await fs.writeFile('./dist/todos.json',JSON.stringify([]));
    }
    return id;
    
}
