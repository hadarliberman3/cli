import { todo } from "../types/todo.js";
import { updateJsonFile } from "../db/database.js";
import { Commands } from "../types/commands.js";
import {printTable, printIsFoundMessage, commandNotFound, newTaskIdMessage} from '../view/output.cli.js';



export function createFunc(myArgs:string[],id:number,myTasks:todo[]){
    let taskStr:string=''+myArgs.shift();
    for(let str of myArgs){
        taskStr+=' '+str;
    }
    let newTask: todo = {id:String(++id), task:taskStr, complete:false};
    myTasks.push(newTask);
    newTaskIdMessage(id);

}

export function deleteFunc(deleteType:string,myTasks:todo[]){
    let flagDelete=false;
    if(deleteType.includes('item')){
        let tasksCount=myTasks.length;
        let idToRemoveArr=deleteType.split('_');
        let idToRemove=idToRemoveArr[idToRemoveArr.length-1];
        myTasks=myTasks.filter((task)=>{
            return task.id!==idToRemove;
         });
            if(myTasks.length==tasksCount) flagDelete=true;
         printIsFoundMessage(flagDelete,idToRemove,'delete');

    }
    else if(deleteType==='completed'){
        myTasks=myTasks.filter((curtask)=>{
            return curtask.complete===false;
         });

    }
    else if(deleteType==='all'){
        myTasks=[];
    }
    else {
        commandNotFound();
    }
    return myTasks;
    
}

export function updateFunc(inputId:string,myTasks:todo[]){
    let flagUpdate=false;
    for(let curtask of myTasks){
        if(curtask.id===inputId){
                curtask.complete=!curtask.complete; 
                flagUpdate=true;     
        }
    }
    printIsFoundMessage(flagUpdate,inputId,'update');
    return myTasks;
}

export function showFunc(command:string,myTasks:todo[]){
    let tasksToShow:todo[]=[];
    if(command==="completed"){
        tasksToShow=myTasks.filter((curtask)=>{
            return curtask.complete===true;
        });
    }
    else if(command==="uncompleted"){
        tasksToShow=myTasks.filter((curtask)=>{
            return curtask.complete===false;
        });
    }
    else if(command==="all"){
        tasksToShow=myTasks;
    }
    else{
        commandNotFound();
    } 
    printTable(tasksToShow);
    return tasksToShow;
    
}

export function isValidCommand(userCommand:string|undefined){
    return (userCommand as string in Commands);

}
export function executeCommand(userCommand:string|undefined,myArgs:string[], id:number, myTasks:todo[]){
    switch (userCommand) {
        case 'create':
            createFunc(myArgs,id,myTasks);
            updateJsonFile(myTasks);
            break;
    
        case 'delete':
            myTasks=deleteFunc(myArgs[0],myTasks);
            updateJsonFile(myTasks);
            break;
    
        case 'update':
            myTasks=updateFunc(myArgs[0],myTasks);
            updateJsonFile(myTasks);
            break;
    
        case 'show':
            showFunc(myArgs[0],myTasks);
            break;
    
            default:
            commandNotFound();
        }
    
}
